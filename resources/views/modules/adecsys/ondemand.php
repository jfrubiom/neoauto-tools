<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="language" content="es">
    <meta name="title" content="NeoAuto Tools v0.1">
    <title>NeoAuto Tools v0.1</title>
    <link href="<?php echo url('/static'); ?>/bootstrap/css/bootstrap.min.css" media="all" rel="stylesheet" type="text/css">
    <link href="<?php echo url('/static'); ?>/css/default.css" media="all" rel="stylesheet" type="text/css">
  </head>
  <body>
    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header"><a class="navbar-brand" href="<?php echo url('/'); ?>">NeoAuto Tools v0.1</a></div>
        <div class="navbar-collapse collapse"></div>
      </div>
    </nav>
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li class="<?php echo App\Library\SidebarMenu::setActive('adecsys_ondemand'); ?>"><a href="<?php echo url('/adecsys/ondemand'); ?>">Reenviar Trama Adecsys</a></li>
            <li class="<?php echo App\Library\SidebarMenu::setActive('adecsys_vignette'); ?>"><a href="<?php echo url('/adecsys/vignette'); ?>">Reenviar Fotos</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1>Reenviar Trama Adecsys</h1>
          <hr>
          <form action="<?php echo url('/adecsys/ondemand-process'); ?>" method="post">
            <div class="form-group">
              <label for="IdAdecsysClasificadoRop">Ingrese los IdAdecsysClasificadoRop:</label>
              <textarea class="form-control" id="IdAdecsysClasificadoRop" name="IdAdecsysClasificadoRop" cols="50" rows="10"></textarea>
            </div>
            <div class="form-group">
              <input class="btn btn-primary form-control" type="submit" value="Enviar">
            </div><?php echo csrf_field(); ?>
          </form>
        </div>
      </div>
    </div>
  </body>
</html>