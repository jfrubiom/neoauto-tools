/**
 * Created by javier on 30/10/16.
 */
var gulp = require('gulp'),
    jeet = require('jeet'),
    rupture = require('rupture'),
    stylus = require('gulp-stylus'),
    urlVersioner = require('gulp-css-url-versioner'),
    autoprefixer = require('autoprefixer'),
    lost = require('lost'),
    postcss = require('gulp-postcss'),
    minifyCss = require('gulp-minify-css');

var paths = require('/usr/local/share/config/paths.js');
var pattern = [
    paths.input.stylus + '/**/*.styl',
    '!' + paths.input.stylus + '/_**/*.styl',
    '!' + paths.input.stylus + '/**/_*.styl'
];

var func = {
    compiler: function(src){
        var dt = require('dateformat'),
            processors = [
                autoprefixer(),
                lost()
            ];

        return gulp.src(src)
            .pipe(stylus({
                use: [rupture(), jeet()]
            }))
            .pipe(postcss(processors))
            .pipe(urlVersioner({version: dt(new Date(), 'yyyymmddHHMMss')}))
            .pipe(minifyCss({keepBreaks: true, keepSpecialComments: '*', benchmark: false}))
            .pipe(gulp.dest(paths.output.docker))
    }
};

gulp.task('css', function(){
    func.compiler(pattern);
});
/*
 gulp.task('html:watch', function(){
 var envWatchFiles = process.env.WATCH_FILES;
 var files = envWatchFiles.split(',');
 var opts = {basedir: paths.core.input.jade};
 var inheritance = new jadeInheritance(paths.core.input.docker + '/' + files, opts.basedir, opts);
 console.log(files);
 });*/