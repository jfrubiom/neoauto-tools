/**
 * Created by javier on 30/10/16.
 */
var gulp = require('gulp'),
    pug = require('gulp-pug'),
    pugLint= require('gulp-pug-lint'),
    rename = require('gulp-rename');

var paths = require('/usr/local/share/config/paths.js');
var pattern = [
    paths.input.jade + '/*.pug',
    paths.input.jade + '/**/*.pug',
    '!' + paths.input.jade + '/_**/*.pug',
    '!' + paths.input.jade + '/**/_*.pug',
    '!' + paths.input.jade + '/**/_**/**/*.pug'
];

var func = {
    compiler: function(src){
        return gulp.src(src, {base: paths.input.jade})
            .pipe(pugLint())
            .pipe(pug({
                pretty: true,
                basedir: paths.input.jade
            }))
            .pipe(rename({extname:'.php'}))
            .pipe(gulp.dest(paths.output.docker));
    }
};

gulp.task('html', function(){
    func.compiler(pattern);
});

gulp.task('html:watch', function(){
    var envWatchFiles = process.env.WATCH_FILES;
    console.log(paths.input.jade + envWatchFiles);
});