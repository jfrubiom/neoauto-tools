/**
 * Created by javier on 19/11/16.
 */
var gulp = require('gulp'),
    coffee = require('gulp-coffee'),
    gzip = require('gulp-gzip'),
    recursiveConcat = require('gulp-recursive-concat');

var paths = require('/usr/local/share/config/paths.js');
var pattern = [
    paths.input.coffee + '/**/*.coffee'
];

var func = {
    compiler: function(src){
        return gulp.src(src)
            .pipe(recursiveConcat({extname: '.js', outside: true}))
            .pipe(coffee({bare: true}))
            .pipe(gulp.dest(paths.output.docker));
    }
};

gulp.task('js', function(){
    func.compiler(pattern);
});