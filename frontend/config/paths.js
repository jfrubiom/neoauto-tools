/**
 * Created by javier on 30/10/16.
 */
var Path = {
    baseDocker: {},
    baseHost: {},
    config: {},
    tasks: {},
    input: {},
    output: {}
};

//base paths
Path.baseDocker = '/usr/local/share';
Path.baseHost = '$(pwd)';

Path.config.docker = Path.baseDocker + '/config';
Path.config.host = Path.baseHost + '/config';

Path.tasks.docker = Path.baseDocker + '/tasks';
Path.tasks.host = Path.baseHost + '/tasks';

//input paths
Path.input.docker = Path.baseDocker + '/input';
Path.input.host = Path.baseHost + '/source';

Path.input.coffee = Path.input.docker + '/coffee';
Path.input.jade = Path.input.docker + '/jade';
Path.input.stylus = Path.input.docker + '/stylus';

//output paths
Path.output.docker = Path.baseDocker + '/output';
Path.output.host = Path.baseHost + '/..';

Path.output.css = Path.output.host + '/public/static/css';
Path.output.js = Path.output.host + '/public/static/js';
Path.output.html = Path.output.host + '/resources/views';

module.exports = Path;