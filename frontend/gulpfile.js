/**
 * Created by javier on 30/10/16.
 */
var
    child_process = require('child_process'),
    gulp = {
        core: require('gulp'),
        path: require('path'),
        util: require('gulp-util')
    },
    paths = require('./config/paths');


gulp.core.task('css', function(callback){
    utils.execute('frontend_css_img', 'css', null, null, paths.output.css);
});

gulp.core.task('html', function(callback){
    utils.execute('frontend_html_img', 'html', null, null, paths.output.html);
});

gulp.core.task('js', function(callback){
    utils.execute('frontend_js_img', 'js', null, null, paths.output.js);
});

gulp.core.task('watch', function(callback){
    console.log(utils.getPath(paths.input.jade));
    gulp.core.watch([utils.getPath(paths.input.jade) + '/**/*.jade'], function(chunk){
        utils.execute('frontend_html_img', 'html', ':watch', utils.getBasePath(chunk.path));
    });
});

var utils = {
    getPath: function(path){
        var baseDir = paths.input.host.split(paths.baseHost);
        var baseInputDir = path.split(paths.input.docker);
        return __dirname + baseDir[1] + baseInputDir[1];
    },

    getBasePath: function(path){
        var baseDir = path.split(paths.input.jade.replace('$(pwd)', __dirname));
        return baseDir[1];
    },

    execute: function(image, task, extras, watchfile, output){
        watchfile = watchfile || '';
        extras = extras || '';
        output = output || paths.output.host;

        if (watchfile !== '') {
            watchfile = ' -e WATCH_FILES="' + watchfile + '"';
        }

        var args = [
            'docker run',
            ' --rm',
            ' -e DEV_UID=', '$(id -u)', ' -e DEV_GID=', '$(id -g)', watchfile,
            ' -v ', paths.config.host, ':', paths.config.docker,
            ' -v ', [paths.tasks.host, '/', 'gulp-', task].join(''), ':', paths.tasks.docker,
            ' -v ', paths.input.host, ':', paths.input.docker,
            ' -v ', output, ':', paths.output.docker,
            ' ', image,
            ' gulp',
            ' ', task,
            extras
        ].join('');

        console.log(args);

        child_process.exec(args, function(err, stdout, stderr){
            if (err) {
                console.log(err);
            } else {
                message = 'Task "' + task + '" finished';
                gulp.util.log(gulp.util.colors.green(message));
            }
        });
    }
};