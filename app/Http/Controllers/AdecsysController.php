<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 21/12/16
 * Time: 21:44
 */

namespace App\Http\Controllers;


use App\Library\Tasker\TaskManager;
use App\Model\Service\Adecsys\ServiceAdecsysApi;
use App\Model\Table\NeoAuto\TableAdecsysClasificadoRop;
use App\Tasks\Vignette\MasterVignette;
use GuzzleHttp\Client as HttpClient;
use Illuminate\Http\Request;

class AdecsysController extends Controller
{
    public function ondemandAction()
    {
        return view('modules.adecsys.ondemand');
    }

    public function ondemandProcessAction(Request $request)
    {
        if ($request->isMethod('post')) {
            set_time_limit(0);

            $rows = [];

            $ids = trim($request->input('IdAdecsysClasificadoRop', ''));
            if (!empty($ids)) {
                $idsCollection = explode(chr(10), $ids);
                foreach ($idsCollection as $id) {
                    $result = $this->_callAdecsysOnDemand($id);

                    if ($result !== false) {
                        $rows[] = $result;
                    }
                }
            }

            dump('Imprimiendo correctos:');
            foreach ($rows as $row) {
                if ($row->error !== 1) {
                    dump($row);
                }
            }

            echo '<br />';
            dump('Imprimiendo fallidos:');
            foreach ($rows as $row) {
                if ($row->error === 1) {
                    dump($row);
                }
            }
        }
    }

    public function vignetteAction()
    {
        return view('modules.adecsys.vignette');
    }

    public function vignetteProcessAction(Request $request)
    {
        if ($request->isMethod('post')) {
            set_time_limit(0);

            $ids = trim($request->input('Cod_Aviso', ''));
            $download = $request->input('downloadOnly', false);

            $tasker = new TaskManager();

            $rows = [];

            if (!empty($ids)) {
                $tableAdecsysClasificadoRop = new TableAdecsysClasificadoRop();
                $idsCollection = explode(chr(10), $ids);
                $row = null;

                foreach ($idsCollection as $id) {
                    $row = $tableAdecsysClasificadoRop->getClasificadoRopByCodAviso($id);

                    if (!empty($row)) {
                        if ($download === false) {
                            $rows[] = [
                                'codAviso' => $row->Cod_Aviso,
                                'response' => $this->_sendHttpRequest($row)
                            ];
                        } else {
                            $filename = '1037908' . $row->Cod_Aviso;
                            $filename = str_pad($filename, 20, '0', STR_PAD_LEFT);

                            $taskVignette = new MasterVignette($row->imageUrl, $row->imageTemplate, $filename);
                            $tasker->addTask($taskVignette, false);
                        }

                    }
                }

                if ($download !== false) {
                    $tasker->runAllTasks();
                }
            }

            $template = 'modules.adecsys.vignette-process';
            if ($download !== false) {
                $template = 'modules.adecsys.vignette-process-download';
                $files = glob(public_path('vignettes/*.jpg'));

                foreach ($files as $file) {
                    $rows[] = str_replace(public_path('vignettes/'), '', $file);
                }
            }

            return view($template, ['rows' => $rows]);
        }
    }

    private function _callAdecsysOnDemand($id=null)
    {
        $urlMask = 'http://neoauto.com/on-demand/adecsys/reenviar-clasificado-eco/id/%s/closed-date/1';
        $url = sprintf($urlMask, $id);

        $client = new HttpClient();
        $response = $client->request('get', $url, ['auth' => ['neoauto_dev', 'Ahph2eThum']]);

        if ($response->getStatusCode() === 200) {
            return $this->_parseReponse($response->getBody()->getContents());
        }

        return false;
    }

    private function _parseReponse($content)
    {
        $response = new \stdClass();

        //obtener idadecsysclasificadorop
        if (preg_match('/\[IdAdecsysclasificadorop\]\ \=\>\ (.*)/', $content, $match)) {
            $response->id = $match[1];
        }

        //obtener cod_aviso
        if (preg_match('/\[Cod\_Aviso\]\ \=\>\ (.*)/', $content, $match)) {
            $response->cod_aviso = $match[1];
        }

        //obtener id_aviso
        if (preg_match('/\[Id\_Aviso\]\ \=\>\ (.*)/', $content, $match)) {
            $response->id_aviso = $match[1];
        }

        //obtener error
        if (preg_match('/\[Error]\ \=\>\ (.*)/', $content, $match)) {
            $response->error = intval($match[1]);
        }

        //obtener mensaje
        if (preg_match('/\[Mensaje]\ \=\>\ (.*)/', $content, $match)) {
            $response->message = $match[1];
        }

        //obtener adecsys
        if (preg_match('/\[Id]\ \=\>\ (.*)/', $content, $match)) {
            $response->adecsys = $match[1];
        }

        return $response;
    }

    public function _sendHttpRequest($row)
    {
        $params = [
            'idPortal' => 'NEOAUTO',
            'idService' => 'SERV_PROCESAR_VINETA',
            'imageUrl' => $row->imageUrl,
            'imageTemplate' => $row->imageTemplate,
            'filename' => str_pad('1037908' . $row->Cod_Aviso, 20, '0', STR_PAD_LEFT),
            'Cod_Aviso' => $row->Cod_Aviso,
            'uploadFtp' => 0
        ];

        return ServiceAdecsysApi::getInstance()->onDemandVineta($params);
    }
}