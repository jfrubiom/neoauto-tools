<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 21/12/16
 * Time: 20:57
 */

namespace App\Http\Controllers;


class HomeController extends Controller
{
    public function indexAction()
    {
        return view('modules.home.index');
    }
}