<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/16
 * Time: 23:46
 */

namespace App\Tasks\Vignette;


use App\Library\Tasker\TaskAbstract;
use App\Library\Tasker\TaskManager;
use App\Tasks\DownloadImage;

class MasterVignette extends TaskAbstract
{
    private $_image;
    private $_template;
    private $_filename;

    public function __construct($image, $template, $filename)
    {
        $this->_image = $image;
        $this->_template = $template;
        $this->_filename = $filename;
    }

    public function run()
    {
        $imageDestination = public_path('vignettes/') . $this->_filename . '-img.jpg';
        $templateDestination = public_path('vignettes/') . $this->_filename . '-template.jpg';
        $destination = public_path('vignettes/') . $this->_filename . '.jpg';

        $taskImageDownload = new DownloadImage($this->_image, $imageDestination);
        $taskTemplateDownload = new DownloadImage($this->_template, $templateDestination);
        $taskGenerateVignette = new GenerateVignette($imageDestination, $templateDestination, $destination);
        $taskApplyCMYKFilter = new ApplyCMYKFilter($destination, $destination);

        $tasker = new TaskManager();
        $tasker
            ->addTask($taskImageDownload)
            ->addTask($taskTemplateDownload)
            ->addTask($taskGenerateVignette)
            ->addTask($taskApplyCMYKFilter);


        $result = $tasker->runAllTasks();

        @unlink($imageDestination);
        @unlink($templateDestination);

        if ($result !== true) {
            $this->_error = $tasker->getErrors();
        }

        return $result;
    }
}