<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 22/12/16
 * Time: 01:32
 */

namespace App\Tasks\Vignette;


use App\Library\Tasker\TaskAbstract;
use App\Library\Vignette\Vignette;

class GenerateVignette extends TaskAbstract
{
    private $_image = null;
    private $_template = null;
    private $_destination = null;
    private $_brightness = 0;

    public function __construct($image, $template, $destination, $brightness=25)
    {
        $this->_image = $image;
        $this->_template = $template;
        $this->_destination = $destination;
        $this->_brightness = $brightness;
    }

    public function run()
    {
        try {
            if (Vignette::createVignette($this->_image, $this->_template, $this->_destination, $this->_brightness)) {
                return true;
            }
        } catch (\Exception $e) {
            $this->_error = $e->getMessage();
        }
        return false;
    }
}