<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 22/12/16
 * Time: 01:34
 */

namespace App\Tasks\Vignette;


use App\Library\Tasker\TaskAbstract;
use App\Library\Vignette\Vignette;

class ApplyCMYKFilter extends TaskAbstract
{
    private $_source = null;
    private $_destination = null;

    public function __construct($source, $destination)
    {
        $this->_source = $source;
        $this->_destination = $destination;
    }

    public function run()
    {
        try {
            if (Vignette::convertToCMYK($this->_source, $this->_destination)) {
                return true;
            }
        } catch (\Exception $e) {
            $this->_error = $e->getMessage();
        }
        return false;
    }
}