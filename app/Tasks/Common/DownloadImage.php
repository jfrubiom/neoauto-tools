<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/16
 * Time: 23:39
 */

namespace App\Tasks;


use App\Library\Tasker\TaskAbstract;

class DownloadImage extends TaskAbstract
{
    private $_source;
    private $_destination;

    public function __construct($source=null, $destination=null)
    {
        $this->_source = $source;
        $this->_destination = $destination;
    }

    public function run()
    {
        try {
            $content = file_get_contents($this->_source);
            $image = imagecreatefromstring($content);
            imagejpeg($image, $this->_destination, 100);
            chmod($this->_destination, 0755);
            return true;
        } catch (\Exception $e) {
            $this->_error = $e->getMessage();
        }

        return false;
    }
}