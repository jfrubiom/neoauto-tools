<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 22/12/16
 * Time: 00:45
 */

namespace App\Model\Service\Adecsys;


use GuzzleHttp\Client as HttpClient;

class ServiceAdecsysApi
{
    static $_instance = null;

    private $_adecsysEndpointUrl = null;
    private $_httpClient = null;

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct()
    {
        $this->_adecsysEndpointUrl = env('ADECSYS_ENDPOINT', 'http://adecsys.orbis.pe');
        $this->_httpClient = new HttpClient();
    }

    public function onDemandVineta($args)
    {
        return $this->call('on-demand-vineta', $args);
    }

    public function call($service, $params=[])
    {
        $response = $this->_httpClient->request('POST', $this->_adecsysEndpointUrl . '/' . $service, ['form_params' => $params]);
        return empty($response->getBody()) ? false : $response->getBody()->getContents();
    }
}