<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 21/12/16
 * Time: 22:40
 */

namespace App\Model\Table\NeoAuto;


use Illuminate\Database\Eloquent\Model;

class TableAdecsysClasificadoRop extends Model
{
    protected $table = 'npc_adecsysclasificadorop';

    public function getClasificadoRopByCodAviso($codAviso=false)
    {
        $row = $this
            ->where('Cod_Aviso', '=', trim($codAviso))
            ->orderBy('Creado', 'desc')
            ->first();

        return empty($row) ? false : $row;
    }
}