<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 21/12/16
 * Time: 21:38
 */

namespace App\Library;


use Illuminate\Support\Facades\Route;

class SidebarMenu
{
    public static function setActive($routeName=null)
    {
        $route = Route::currentRouteName();
        return $route == $routeName ? 'active' : '';
    }
}