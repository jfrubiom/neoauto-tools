<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 21/12/16
 * Time: 00:05
 */

namespace App\Library\Vignette;


use Image;

class Vignette
{
    const PROFILE_SWOP_ICC = 'USWebCoatedSWOP.icc';
    const PROFILE_RGB_ICM = 'sRGBColorSpaceProfile.icm';

    public static function createVignette($image, $template, $destination, $brightness=25)
    {
        try {
            $objImage = Image::make($image)->resize(330, 236);
            $filterBrightness = new BrightnessFilter($brightness);

            Image::make($template)
                ->insert($objImage, 'top-left', 65, 0)
                ->filter($filterBrightness)
                ->save($destination, 100);
            chmod($destination, 0755);
            return true;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public static function convertToCMYK($source, $destination)
    {
        try {
            $profileICM = file_get_contents(base_path('resources/profiles/' . self::PROFILE_RGB_ICM));
            $profileICC = file_get_contents(base_path('resources/profiles/' . self::PROFILE_SWOP_ICC));

            $imagick = new \Imagick($source);
            $imagick->profileImage('icc', $profileICM);
            $imagick->profileImage('icc', $profileICC);
            $imagick->setImageColorspace(\Imagick::COLORSPACE_CMYK);
            $imagick->setImageDepth(8);
            $imagick->setImageCompression(\Imagick::COMPRESSION_JPEG);
            $imagick->setImageCompressionQuality(60);
            $imagick->setSamplingFactors([1, 1, 1, 1]);
            $imagick->setInterlaceScheme(\Imagick::INTERLACE_JPEG);
            $imagick->setCompressionQuality(60);
            $imagick->setImageUnits(\Imagick::RESOLUTION_PIXELSPERINCH);
            $imagick->setImageResolution(300, 300);
            $imagick->setImageInterpolateMethod(\Imagick::INTERPOLATE_SPLINE);
            $imagick->writeImage($destination);
            chmod($destination, 0755);
            return true;
        } catch (\Exception $e) {
            echo $e->getMessage();
            throw $e;
        }
    }
}