<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 21/12/16
 * Time: 00:11
 */

namespace App\Library\Vignette;


use Intervention\Image\Filters\FilterInterface;
use Image;

class BrightnessFilter implements FilterInterface
{
    private $_brightness = 25;

    public function __construct($brightness)
    {
        $this->_brightness = $brightness;
    }

    public function applyFilter(\Intervention\Image\Image $image)
    {
        // TODO: Implement applyFilter() method.
        $resImage = $image->getCore();
        imagefilter($resImage, IMG_FILTER_BRIGHTNESS, $this->_brightness);
        return Image::make($resImage);
    }
}