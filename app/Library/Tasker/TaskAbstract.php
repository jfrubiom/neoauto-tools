<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/16
 * Time: 23:36
 */

namespace App\Library\Tasker;


abstract class TaskAbstract implements TaskInterface
{
    protected $_error;

    public function getError()
    {
        return $this->_error;
    }
}