<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/16
 * Time: 23:23
 */

namespace App\Library\Tasker;


interface TaskInterface
{
    public function run();
    public function getError();
}