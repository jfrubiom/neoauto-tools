<?php
/**
 * Created by PhpStorm.
 * User: javier
 * Date: 20/12/16
 * Time: 23:22
 */

namespace App\Library\Tasker;


class TaskManager
{
    protected $_tasks;
    protected $_errors;

    public function __construct()
    {
        $this->_tasks = [];
        $this->_errors = [];
    }

    /**
     * @param TaskInterface $task
     * @param bool $breakOnFail
     * @return $this
     */
    public function addTask(TaskInterface $task, $breakOnFail=false)
    {
        $this->_tasks[] = [
            'task' => $task,
            'breakOnFail' => $breakOnFail
        ];

        return $this;
    }

    public function runAllTasks()
    {
        $result = true;

        foreach ($this->_tasks as $element) {
            /**
             * @var $task TaskInterface
             */
            $task = $element['task'];
            if ($task instanceof TaskInterface && $task->run()) {
                continue;
            }

            $result = false;
            $this->_errors[] = $task->getError();

            if ($element['breakOnFail'] === true) {
                break;
            }
        }

        return $result;
    }

    public function getErrors()
    {
        print_r($this->_errors);
    }
}