<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Route::get('/', 'HomeController@indexAction');

Route::group(['prefix' => '/adecsys'], function(){
    Route::get('/ondemand', 'AdecsysController@ondemandAction')->name('adecsys_ondemand');
    Route::post('/ondemand-process', 'AdecsysController@ondemandProcessAction');

    Route::get('/vignette', 'AdecsysController@vignetteAction')->name('adecsys_vignette');
    Route::post('/vignette-process', 'AdecsysController@vignetteProcessAction')->name('adecsys_vignette');
});